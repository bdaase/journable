# utils.py
#
# Copyright 2020 Björn Daase
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Find children by names becuase ids are not always set?

from gi.repository import Gtk


def find_child(parent, name):
    if parent.get_name() == name:
        return parent

    if isinstance(parent, Gtk.Container):
        children = parent.get_children()
        for child in children:
            widget = find_child(child, name)
            if widget is not None:
                return widget

    return None
