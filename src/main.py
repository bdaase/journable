# main.py
#
# Copyright 2020 Björn Daase
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '4')

from .window import JournableWindow

from gi.repository import Gtk, GtkSource, Gio, GObject


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='net.daase.journable',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_activate(self):
        GObject.type_register(GtkSource.View)
        win = self.props.active_window
        if not win:
            win = JournableWindow(application=self)
        win.present()

    def do_startup(self):
        Gtk.Application.do_startup(self)

        quit_action = Gio.SimpleAction.new('quit', None)
        quit_action.connect('activate', self._quit)
        self.add_action(quit_action)
        self.set_accels_for_action('app.quit', ['<Ctrl>Q', '<Ctrl>W'])

        save_action = Gio.SimpleAction.new('save', None)
        save_action.connect('activate', self._save)
        self.add_action(save_action)
        self.set_accels_for_action('app.save', ['<Ctrl>S'])

        save_as_action = Gio.SimpleAction.new('save_as', None)
        save_as_action.connect('activate', self._save_as)
        self.add_action(save_as_action)
        self.set_accels_for_action('app.save_as', ['<Ctrl><Shift>S'])

        new_action = Gio.SimpleAction.new('new', None)
        new_action.connect('activate', self._new)
        self.add_action(new_action)
        self.set_accels_for_action('app.new', ['<Ctrl>N'])

        open_action = Gio.SimpleAction.new('open', None)
        open_action.connect('activate', self._open)
        self.add_action(open_action)
        self.set_accels_for_action('app.open', ['<Ctrl>O'])

    def _quit(self, action, param):
        win = self.props.active_window
        win.close()

    def _save(self, action, param):
        win = self.props.active_window
        win.save_file()

    def _save_as(self, action, param):
        win = self.props.active_window
        win.save_file_as()

    def _new(self, action, param):
        win = self.props.active_window
        win.new_journal()

    def _open(self, action, param):
        win = self.props.active_window
        win.open_file()


def main(version):
    app = Application()
    return app.run(sys.argv)
