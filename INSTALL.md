Building & Installing Journable
===============================

Building
--------

Journable uses the Meson <http://mesonbuild.com> and Ninja
<https://ninja-build.org> build systems. To build Journable, run the
following commands from the top-level directory of the source code
repository:

    meson build
    ninja -C build

A convenience Makefile for development only is also provided. To use
it, simply invoke make from the top-level directory.

Dependencies
------------

Building Journable requires the following major libraries and tools:

 * GTK+ 3
 * GtkSourceView 4

See the `meson.build` file in the top-level directory for the complete
list of required dependencies and minimum versions.

All required libraries and tools are available from major Linux
distribution's package repositories:

Installing dependencies on Fedora
---------------------------------

Install them by running this command:

    sudo dnf install meson python3 gtk3-devel gtksourceview4-devel

Installing dependencies on Ubuntu/Debian
----------------------------------------

Install them by running this command:

    sudo apt-get install meson python3 libgtk-3-dev libgtksourceview-4-dev

Installation
------------

After Journable has built, install it by invoking the install target:

    ninja -C build install

After installation, it can be uninstalled in the same way:

    ninja -C build uninstall

By default, Journable will install under /usr/local. To install to a
different directory, set pass the --prefix to meson when performing
the initial configuration step:

    meson --prefix=/usr -C build
